import os;
from WfUtils import WfUtils;

class WfBuild:
    # This function runs the build process
    @staticmethod
    def build_project(include_lc):
        # Building the project and checking if build is successful or not
        os.chdir(WfUtils.WF_ROOT_PATH);
        os.system("mvn clean install -Dmaven.test.skip=true");
        is_project_build_successful = os.path.isfile(WfUtils.WF_PLUGIN_ZIP_PATH + "/" + WfUtils.WF_PLUGIN_ZIP_NAME);
        
        if is_project_build_successful:
            # Packaging the demo war
            os.chdir(WfUtils.WF_MISC_DEMO_PATH);
            if os.path.isfile(WfUtils.WF_MISC_DEMO_PATH + "/pom.xml"):
                command = "mvn clean package -DpublicationContent=demo";
                if include_lc:
                   command = command + " -DenablePlugin=lc -DpluginContent=lc-demo"; 
                os.system(command);
                is_demo_packaging_successful = os.path.isfile(WfUtils.WF_MISC_DEMO_PATH + "/demo-site/target/" + WfUtils.WF_DEMO_WAR_NAME);
                if not is_demo_packaging_successful:
                    is_demo_packaging_successful = os.path.isfile(WfUtils.WF_MISC_DEMO_PATH + "/target/" + WfUtils.WF_DEMO_WAR_NAME);

            if is_demo_packaging_successful:
                print("widget framework build successful.")
            else:
                WfUtils.show_error("The project packaging failed.")
        else:
            WfUtils.show_error("The project building failed.")
