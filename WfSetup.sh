# Setting initial values.
user_home="/home/$USER/"
publication_home="/opt/escenic/assemblytool/publications"
plugins_home="/opt/escenic/engine/plugins"
tomcat_home="/opt/escenic/engine/tomcat"

# New Git style WF version
warName="demo-site-develop-SNAPSHOT"
pluginName="widget-framework-develop-SNAPSHOT"

if [ $# -eq 0 ]; then
    if [ ! -e "$user_home/$pluginName.zip" ]; then
       # Perforce style WF version
       pluginName="widget-framework-core-trunk-SNAPSHOT"
       warName="demo-site-trunk-SNAPSHOT"
    fi
else
    # WF spefic branch SNAPSHOT
    pluginName="widget-framework-core-$1-SNAPSHOT"
    warName="demo-site-$1-SNAPSHOT"
fi

if [ ! -e "$user_home/$warName.war" ]; then
    # Considering for legacy style WF publication
    warName="${warName//site/core}"
fi

# A final check
if [ -e "$user_home/$pluginName.zip" ] && [ -e "$user_home/$warName.war" ]; then 
    echo "Processing WF version: $(echo "$warName" | cut -d'-' -f 3)"
else
  printf "Some required files not found. Check if: \n 1) Publication WAR exists \n 2) Plugins ZIP exists \n 3) They have the same version"
  exit 1
fi

echo "Starting ECE server assembling and redeploying  process"
echo "--------------------------------------------------------"

cd "$user_home" || exit
unzip -qq "$pluginName.zip"

echo "***Removing old core plugins and copying new one"
rm -rf  $plugins_home/widget-framework-*
mv -f "$pluginName" "$plugins_home"
echo "---Done."

echo "***Removing old publication WAR and copying new one"
publicationWar="demo"
if [ ! "$2" = '' ]; then
  publicationWar="$2";
fi
  
cp -f "$warName.war" "$publication_home/$publicationWar.war"
echo "---Done."

echo "***Stopping server and cleaning up tomcat"
ece stop
#tomcat cleanup
rm -rf $tomcat_home/work/Catalina/localhost/*
rm -rf $tomcat_home/webapps/*
rm -rf $tomcat_home/temp/*
rm $tomcat_home/logs/localhost_access_log*
rm $tomcat_home/logs/host-manager*
rm $tomcat_home/logs/catalina*
rm $tomcat_home/logs/manager*
cp -rf ~/defaults-webapp/* $tomcat_home/webapps
echo "---Done."

echo "Removing the unneeded zip file and folder from home."
rm "$pluginName.zip"
rm "$warName.war"
echo "***Invoking the ECE processes."
ece flush clean assemble deploy start
