import sys, getopt, os;
from WfUtils import WfUtils;
from WfBuild import WfBuild;

HELP_TEXT = "\nThis is the Widget framework build script. Available options are:\n" \
            "   -h                 help message\n" \
            "   -b, --build        build the widget framework project, according to version specified..\n" \
            "   -l, --live-center  build the widget framework project, according to version specified including live center.\n" \
            "   -v, --version      widget framework version number. Default: develop\n" \
            "   -s, --server       The server hostname where to copy the Plugins ZIP and demo WAR files\n" \
            "Example:\n" \
            "   WfMain.py -b -vVERSION -sSERVER_NAME\n" \
            "   WfMain.py --build --version=VERSION --server=SERVER_NAME\n";

# Starting main() method
def main(arguments):
    server = None;
    enable_build = False;
    include_lc = False;
    copy_to_server = False;
    
    # checking console inputs and handling invalid inputs
    try:
        opts, args = getopt.getopt(arguments, "hblv:s:", ["version=", "server=", "build", "live-center"]);
    except getopt.GetoptError:
        WfUtils.show_error("Specify proper input.\n", HELP_TEXT);

    # Iterating through the options provided in console
    for opt, arg in opts:
        if opt == "-h":
            print(HELP_TEXT);
            sys.exit();

        elif opt in ("-b", "--build"):
            enable_build = True;

        elif opt in ("-l", "--live-center"):
            enable_build = True;
            include_lc = True;

        elif opt in ("-v", "--version"):
            WfUtils.VERSION = WfUtils.validate_input(arg, "Version").lower();

        elif opt in ("-s", "--server"):
            server = WfUtils.validate_input(arg, "Server name");
            copy_to_server = server is not None or server is not "";

        else:
            print("Please provide necessary parameter.\n");
            print(HELP_TEXT);
            sys.exit(2);

    # Updates WF relevant paths according to version value.
    WfUtils.update_paths();
    
    # Start build process
    if enable_build:
        if include_lc:
            WfBuild.build_project(True);
        else:
            WfBuild.build_project(False);

    # if server is specified the copying required files to server for deployment
    if copy_to_server:
        server_address = "escenic@" + server;
        # Copying plugin zip file
        WfUtils.copy_file_to_server(WfUtils.WF_PLUGIN_ZIP_PATH, WfUtils.WF_PLUGIN_ZIP_NAME, server_address);

        # Copying demo war file
        if os.path.exists(WfUtils.WF_MISC_DEMO_PATH + "/demo-site/target"):
            WfUtils.copy_file_to_server(WfUtils.WF_MISC_DEMO_PATH + "/demo-site/target/", WfUtils.WF_DEMO_WAR_NAME, server_address);
        else: 
            WfUtils.copy_file_to_server(WfUtils.WF_MISC_DEMO_PATH + "/target/", WfUtils.WF_DEMO_WAR_NAME, server_address);
        # Running deployment script on remote server
        #os.system("ssh " + server_address);
##############################################################################################################

# The following block tells the interpreter to start from main()
if __name__ == "__main__":
    main(sys.argv[1:]);

