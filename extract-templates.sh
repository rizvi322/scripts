#!/bin/bash

if [[ "$#" -ne 1 ]]; then echo "Parameter missing: Publication name"; exit; fi

pubName=$1
server=localhost

runDBScript(){
  if [[ $# -gt 0 ]]; then
    echo $1 > /tmp/scrip.sql
    echo `mysql -u root -p1234 ecedb < /tmp/scrip.sql | grep -oE '^[0-9]+'`
  else
    echo ""
  fi
}

depth=0
getSubSections(){
  depth=$((depth + 1))
  for sec; do
    echo "SELECT sectionID, uniqueName FROM Section WHERE sectionID IN (SELECT Sec_sectionID FROM Sec_Ref WHERE sectionID = $sec);" > /tmp/scrip.sql
    subSecs=`mysql -u root -p1234 ecedb < /tmp/scrip.sql | tr -s '\t' ':' | grep -E "^[0-9]+"`
    for subsec in $subSecs; do
      echo $depth:$subsec
      echo `getSubSections ${subsec%%:*}`
    done
  done
  depth=$((depth - 1))
}

pubId=`runDBScript "SELECT referenceID FROM Publication WHERE publicationName = '$pubName';"`
if [[ -z $pubId ]]; then echo "Publication not found!"; exit; fi

configSecId=`runDBScript "SELECT sectionID FROM Section WHERE referenceID = $pubId AND uniqueName = 'config';"`
if [[ -z $configSecId ]]; then echo "Config section not found!"; exit; fi

ARRAY=(
0:$configSecId:config
`getSubSections $configSecId`
)

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
MIG_FILE="/tmp/export/000001_migration.xml"
EXPORT_DIR="$DIR/../content"
mkdir -p $EXPORT_DIR

echo "Creating section XMLs in dir: $EXPORT_DIR"
for section in "${ARRAY[@]}" ; do
    depth=${section%%:*}
    id=`echo $section | sed 's/\(.*\):\(.*\):\(.*\)/\2/'`
    uniqename=${section##*:}
    out_file="$EXPORT_DIR/$depth.$uniqename.xml"
    curl -O -s "http://$server:8080/escenic-admin/do/publication/export" --data "publicationId=$pubId&sectionIds=$id&directoryName=%2Ftmp%2Fexport%2F&maxNumberOfObjects=0&sections=on&pools=on&articles=on&fromDate=&toDate=&Submit=Export&confirmed=true"
    if [ -f $MIG_FILE ]
    then
      cp $MIG_FILE $out_file
      rm $MIG_FILE
      perl -0777 -p -i -e 's/\<inbox.*?\<\/inbox>//sg' $out_file
      perl -p -i -e 's/\<inbox.*?\/\>//g' $out_file
      perl -p -i -e 's/(\<parent )source=\".*?\" sourceid=\".*?\" /$1/g' $out_file
      perl -p -i -e 's/(\<section unique-name=\".*?\" )source=\".*?\" sourceid=\".*?\" /$1/g' $out_file
      perl -p -i -e 's/(\<section-page )source=\".*?\" sourceid=\".*?\" /$1/g' $out_file
      perl -p -i -e 's/creationdate=\".*?\"//g' $out_file
      perl -p -i -e 's/publishdate=\".*?\"//g' $out_file
      perl -p -i -e 's/last-modified=\".*?\"//g' $out_file
      perl -p -i -e 's/activatedate=\".*?\"//g' $out_file
      perl -p -i -e 's/(\<section-page .*?)\>/$1 action=\"remove\"\>/g' $out_file
      perl -p -i -e 's/exported-dbid=\".*?\"//g' $out_file
      perl -p -i -e 's/<uri>.*?<\/uri>//g' $out_file
      perl -p -i -e 's/<creator>.*?<\/uri>//g' $out_file
      perl -p -i -e 's/<author>.*?<\/uri>//g' $out_file
      xmllint --output $out_file --format $out_file
      echo "$out_file"
    fi
done

# cleanup
rm -rf `pwd`/export
