import sys, os;

class WfUtils:
    
    VERSION = "develop";
    WF_ROOT_PATH_SUFFIX_FOR_GIT = "/Workspace/projects/stash/widget-framework"
    WF_ROOT_PATH_SUFFIX_FOR_PERFORCE = "/Workspace/projects/perforce/escenic/widget-framework"
    WF_ROOT_PATH = "/home/" + os.getenv("USER", "raihan") + WF_ROOT_PATH_SUFFIX_FOR_GIT;
    
    WF_PLUGIN_ZIP_NAME = ""
    WF_PLUGIN_ZIP_PATH = "/widget-framework-dist/framework-dist/target";
    
    WF_DEMO_WAR_NAME = ""
    WF_MISC_DEMO_PATH = "";
    
    ############################################################################################
    
    # This function validates input string and exit program for invalid String
    @staticmethod
    def validate_input(value, label):
        if value is None or value == "" or str(value).strip().startswith("-"):
            WfUtils.show_error(label + " is not specified.");
        else:
            return str(value).strip();
    #############################################################################################
    
    # This function updates the root path according to version
    @staticmethod
    def update_paths():
        temp = "widget-framework-develop-SNAPSHOT";
        if WfUtils.VERSION != "develop":
            WfUtils.WF_ROOT_PATH = "/home/" + os.getenv("USER", "raihan") + WfUtils.WF_ROOT_PATH_SUFFIX_FOR_PERFORCE;

            if WfUtils.VERSION == "trunk":
                WfUtils.WF_ROOT_PATH += "/" + WfUtils.VERSION;
                WfUtils.VERSION = "develop"
            else:
                WfUtils.WF_ROOT_PATH += "/branches/" + WfUtils.VERSION;
                temp = "widget-framework-core-" + WfUtils.VERSION  + "-SNAPSHOT";
                
        if os.path.exists(WfUtils.WF_ROOT_PATH):
            # Updating plugins zip path and name
            WfUtils.WF_PLUGIN_ZIP_PATH = WfUtils.WF_ROOT_PATH + WfUtils.WF_PLUGIN_ZIP_PATH;
            WfUtils.WF_PLUGIN_ZIP_NAME = temp + ".zip";
    
            # Updating misc-demo path
            WfUtils.WF_MISC_DEMO_PATH = WfUtils.WF_PLUGIN_ZIP_PATH + ("/" + temp) * 2 + "/misc/demo";
            if os.path.isfile(WfUtils.WF_MISC_DEMO_PATH + "/" + "pom.xml") and os.path.exists(WfUtils.WF_MISC_DEMO_PATH + "/demo-site"):
                WfUtils.WF_DEMO_WAR_NAME = "demo-site-" + WfUtils.VERSION + "-SNAPSHOT.war";
            else:
                WfUtils.WF_DEMO_WAR_NAME = "demo-core-" + WfUtils.VERSION + "-SNAPSHOT.war";
    
        else:
            WfUtils.show_error("According to the version specified, the Project root path not found.");
    ##################################################################################################
    
    # This function copy a files to the server
    @staticmethod
    def copy_file_to_server(path, name, server_address):
        if str(server_address).find(":") == -1:
            server_address += ":";
            
        if os.path.isfile(path+ "/" + name):
            #Copying file to server
            os.chdir(path);
            command = "scp -r " + name + " " + server_address;
            print(command);
            os.system(command);
        else:
            WfUtils.show_error("File not found.\n", path + "/" + name);
    ##############################################################################################################

    # This function prints error message and exit the program
    @staticmethod
    def show_error(message):
        print(message);
        sys.exit(2);
    ##############################################################################################
